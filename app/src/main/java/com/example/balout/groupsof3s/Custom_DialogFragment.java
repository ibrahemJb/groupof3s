package com.example.balout.groupsof3s;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class Custom_DialogFragment extends DialogFragment{
    private MyDialogListener myDialogListener;

    public void setMyDialogListener(MyDialogListener myDialogListener) {
        this.myDialogListener = myDialogListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        try {
            Dialog dialog = super.onCreateDialog(savedInstanceState);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View v = getActivity().getLayoutInflater().inflate(R.layout.add_dialog,null);
            TextView s = v.findViewById(R.id.tv_add);
            s.setText(getArguments().getString("title"));
            Button add = v.findViewById(R.id.button);
            final EditText et = v.findViewById(R.id.et_add);
            et.setInputType(getArguments().getInt("inputType"));
            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myDialogListener.userSelectedAValue(et.getText().toString());
                }
            });
            Button caButton = v.findViewById(R.id.cancle);
            caButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            builder.setView(v);

            return builder.create();
        }
        catch (Exception ex){
            return null;
        }
    }

}
