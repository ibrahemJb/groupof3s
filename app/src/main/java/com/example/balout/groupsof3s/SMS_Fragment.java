package com.example.balout.groupsof3s;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.telephony.SmsManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class SMS_Fragment extends  Fragment {
    public static final int DSMS_REQUEST=100;
    public Custom_DialogFragment custom_dialogFragment;
    String numbers[]=null;
    String messageText = "";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sms, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ImageView add = getView().findViewById(R.id.imageView);
        ImageView remove = getView().findViewById(R.id.remove);
        final TextView sendToNumbers = getView().findViewById(R.id.SendToNumbers);
        FloatingActionButton fab = getView().findViewById(R.id.fab);
        final EditText editText = getView().findViewById(R.id.editText);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!sendToNumbers.getText().equals("None") && !editText.getText().toString().isEmpty()){
                    messageText = editText.getText().toString();
                    numbers = sendToNumbers.getText().toString().split("\n");
                    if(ActivityCompat.checkSelfPermission(getContext(),Manifest.permission.SEND_SMS)!=PackageManager.PERMISSION_GRANTED)
                        ActivityCompat.requestPermissions ( getActivity(), new String[]{
                                Manifest.permission.SEND_SMS}, DSMS_REQUEST );
                    else{
                        sendSMS();
                    }
                }
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                custom_dialogFragment = new Custom_DialogFragment ();
                Bundle b = new Bundle();
                b.putString("title","Add Number");
                b.putInt("inputType", InputType.TYPE_CLASS_PHONE);
                custom_dialogFragment.setArguments(b);
                custom_dialogFragment.setMyDialogListener(new MyDialogListener() {
                    @Override
                    public void userSelectedAValue(String value) {
                        TextView v = getView().findViewById(R.id.SendToNumbers);
                        if(v.getText().equals("None")){
                            v.setText(value);
                        }
                        else{
                            String s= v.getText()+"\n"+value;
                            v.setText(s);
                        }
                        custom_dialogFragment.dismiss();
                    }
                });
                FragmentManager fragmentManager = getFragmentManager();
                custom_dialogFragment.show(fragmentManager, "dialog");
            }
        });
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        super.onViewCreated(view, savedInstanceState);
    }
    @Override
    public  void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode==DSMS_REQUEST && grantResults.length==2 &&
                grantResults[0]== PackageManager.PERMISSION_GRANTED &&
                grantResults[1]==PackageManager.PERMISSION_GRANTED) {
                sendSMS();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void sendSMS(){
        SmsManager man=SmsManager.getDefault ();
        for (String number:numbers) {
            man.sendTextMessage ( number, null, messageText, null, null );
        }
    }
}
